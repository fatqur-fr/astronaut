module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily:{
        // font-family: 'Chivo', sans-serif;
        // font-family: 'DM Sans', sans-serif;
        'chivo': ['Chivo', 'sans-serif'],
        'dm-sans': ["DM Sans", 'sans-serif']
        // 'MyFont': ['"My Font"', 'serif'] // Ensure fonts with spaces have " " surrounding it.
        
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
